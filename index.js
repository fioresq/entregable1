

function guardarSessionStorage() {
  var inputClave = document.getElementById("inputFormClave"); /* Referencia al input de clave */
  var inputValor = document.getElementById("inputFormValor"); /* Referencia al input de valor */
  var clave = inputClave.value;
  var valor = inputValor.value;
  sessionStorage.setItem(clave, valor);
}

function leerSessionStorage() {
  var inputClave = document.getElementById("inputFormClave");
  var inputValor = document.getElementById("inputFormValor"); 
  var clave = inputClave.value;
  var valor = sessionStorage.getItem(clave);
    inputValor.value = valor;
	refrescarTablas();
}

function eliminarSessionStorage() {
  var inputClave = document.getElementById("inputFormClave"); /* Referencia al input de clave */
  var clave = inputClave.value;
	sessionStorage.removeItem(clave);
	refrescarTablas();
}
function limpiarSessionStorage(){
	sessionStorage.clear();
	refrescarTablas();
}

function elementosSessionStorage(){
	alert('EL número de elementos es: '+ sessionStorage.length);	
}

function refrescarTablas() {
  refrescarTablaSessionStorage();
}

function refrescarTablaSessionStorage() {
  console.log("Refrescando Tabla de SessionStorage");
  let arraySessionStorage=sessionStorage;
  let TablaSessionStorage= document.getElementById('table-body-session-storage');

  TablaSessionStorage.innerHTML="";
  for(let property in arraySessionStorage){
    if(property!='length' && property!='key' & property!='getItem' && property!='setItem' && property!='removeItem' && property!='clear' && property!='' && property!='_$_'){
      console.log(property);
      TablaSessionStorage.innerHTML= TablaSessionStorage.innerHTML +`<tr><td>${property}</td><td>${arraySessionStorage.getItem(property)}</td></tr> `
    }
  }
}